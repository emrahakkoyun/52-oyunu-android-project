package com.gojolo.oyunu52;


import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {
	Handler hd;
	int count=0;
	Boolean tb=true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		hd= new Handler();
		Thread  t = new Thread(new Runnable() {
			@Override
			public void run() {
				while(tb==true)
				{
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					hd.post(new Runnable() {
						@Override
						public void run() {
							if(count>0)
							{

							}
							else{
								tb=false;
							}
							count--;
						}
					});

				}
				if(tb==false)
				{
					startActivity(new Intent(MainActivity.this,Game.class));
				}
			}
		});
		t.start();

	}
	}
