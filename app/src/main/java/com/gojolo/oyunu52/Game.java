package com.gojolo.oyunu52;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
public class Game extends Activity implements  View.OnTouchListener {
	public static Integer[] kartlar = new Integer[]  {
		R.drawable.k1,
		R.drawable.k2,
		R.drawable.k3,
		R.drawable.k4,
		R.drawable.k5,
		R.drawable.k6,
		R.drawable.k7,
		R.drawable.k8,
		R.drawable.k9,
		R.drawable.k10,
		R.drawable.kj,
		R.drawable.kz,
		R.drawable.kk,
		R.drawable.s1,
		R.drawable.s2,
		R.drawable.s3,
		R.drawable.s4,
		R.drawable.s5,
		R.drawable.s6,
		R.drawable.s7,
		R.drawable.s8,
		R.drawable.s9,
	    R.drawable.s10,
		R.drawable.sj,
		R.drawable.sz,
		R.drawable.sk,
		R.drawable.z1,
		R.drawable.z2,
		R.drawable.z3,
		R.drawable.z4,
		R.drawable.z5,
		R.drawable.z6,
		R.drawable.z7,
		R.drawable.z8,
		R.drawable.z9,
		R.drawable.z10,
		R.drawable.zj,
		R.drawable.zz,
		R.drawable.zk,
		R.drawable.y1,
		R.drawable.y2,
		R.drawable.y3,
		R.drawable.y4,
		R.drawable.y5,
		R.drawable.y6,
		R.drawable.y7,
		R.drawable.y8,
		R.drawable.y9,
		R.drawable.y10,
		R.drawable.yj,
		R.drawable.yz,
		R.drawable.yk
};
	int screenwidth,screenheight;
	static int kartsayi=52,bkart;
	String[] robotnames={"Alex","Martin","Jhon","Tom"};
	RelativeLayout.LayoutParams kartboyut,yaziboyut;
	RelativeLayout rlt;
	double[] cord={1.75,0,2.10,0,2.55,0,3.40,0,1.80,3.80,2.10,3.80,2.55,3.80,3.20,3.80,1.80,-4.40,2.10,-4.40,2.55,-4.40,3.20,-4.40};
	int cordcounter=0;
	TextView kartsayisi,robotname,youname;
	Button kart;
	Button[] kart1 = new Button[12];
private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		rlt=(RelativeLayout)findViewById(R.id.rlt);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		screenwidth = size.x;
		screenheight = size.y;
		cord[1]=screenheight;
		cord[3]=screenheight;
		cord[5]=screenheight;
		cord[7]=screenheight;
		rlt.setOnTouchListener(this);
		AdRequest adRequest = new AdRequest.Builder().build();
		interstitial = new InterstitialAd(Game.this);
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/8703906642");
		interstitial.loadAd(adRequest);
		kartboyut = new RelativeLayout.LayoutParams(
				screenwidth/15,
				screenheight/8);
		yaziboyut = new RelativeLayout.LayoutParams(
				screenwidth/20,
				screenheight/10);
	    kart = new Button(this);
		kartsayisi= new TextView(this);
		robotname = new TextView(this);
		youname=new TextView(this);
		robotname.setText(String.valueOf(robotnames[0]));
		youname.setText("you");
		robotname.setX((float)(screenwidth/1.90));
		robotname.setY((float) (screenheight/90));
		youname.setX((float)(screenwidth/1.90));
		youname.setY((float) (screenheight/1.29));
		kartsayisi.setText(String.valueOf(kartsayi));
		kartsayisi.setX(screenwidth/17);
		kartsayisi.setY((float) (screenheight/4.5));
		kartsayisi.setTextColor(Color.parseColor("#042409"));
		kart.setBackgroundResource(R.drawable.bos);
		kart.setX(screenwidth/24);
		kart.setY((float) (screenheight/3.0));
		rlt.addView(kart,kartboyut);
		rlt.addView(kartsayisi,yaziboyut);
		rlt.addView(youname,yaziboyut);
		rlt.addView(robotname,yaziboyut);
	}
	public void displayInterstitial() {
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		float x = (float) event.getX();
		float y = (float) event.getY();

		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				Log.i("TAG", "touched down");
				break;
			case MotionEvent.ACTION_MOVE:
				Log.i("TAG", "moving: (" + screenwidth/x + ", " + screenheight/y + ")");
				break;
			case MotionEvent.ACTION_UP:
				if(screenwidth/x>9.252&&screenwidth/x<112.184&&screenheight/y>1.23&&screenheight/y<1.39)
				{
					Toast.makeText(getApplicationContext(),"basladi",Toast.LENGTH_LONG).show();
					startcard();
				}
				break;
		}

		return true;
	}
	public void startcard()
	{
		bkart=52-kartsayi;
		kartsayisi.setText(String.valueOf(kartsayi));
		this.kart1[0].setBackgroundResource(R.drawable.bos);
		this.kart1[0].setX(screenwidth/24);
		this.kart1[0].setY((float) (screenheight/3.0));
		rlt.addView(this.kart1[0],kartboyut);
		AnimationSet replaceAnimation = new AnimationSet(false);
		replaceAnimation.setFillAfter(true);
		TranslateAnimation trans = new TranslateAnimation(0, 0,
		TranslateAnimation.ABSOLUTE, (float)(screenwidth/cord[cordcounter]), 0, 0,
				TranslateAnimation.ABSOLUTE,(float)(screenheight/cord[cordcounter+1]));
		trans.setDuration(1000);
		replaceAnimation.addAnimation(trans);
		kart1[0].startAnimation(replaceAnimation);
		cordcounter+=2;
		kartsayi--;
	}
}
